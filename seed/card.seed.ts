export const CardSeed = [
    {
        number: "4532993514324743",
        cvc: 123,
        type: "Visa",
        user: { id: 2 }
    },
    {
        number: "4024007142181592",
        cvc: 123,
        type: "Visa",
        user: { id: 2 }
    },
    {
        number: "4485225961309324",
        cvc: 123,
        type: "Visa",
        user: { id: 3 }
    },
    {
        number: "5528050553131720",
        cvc: 123,
        type: "MasterCard",
        user: { id: 2 }
    },
    {
        number: "5191824138283859",
        cvc: 123,
        type: "MasterCard",
        user: { id: 3 }
    },
];