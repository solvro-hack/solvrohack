
export const CategorySeed = [
    {
        categoryName: "Rozrywka",
        icon: "gamepad"
    },
    {
        categoryName: "Video",
        icon: "video"
    },
    {
        categoryName: "Muzyka",
        icon: "headphones"
    }
]