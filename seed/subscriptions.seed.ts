export const SubscriptionSeed = [
    {
        service: { id: 1 },
        category: {id: 1},
        card: { id: 1 },
        limit: 80.00,
        interval: "1m",
        amount: 49.99
    },
    {
        service: { id: 2 },
        category: {id: 1},
        card: { id: 1 },
        limit: 35.00,
        interval: "1m",
        amount: 34.99
    },
    {
        service: { id: 4 },
        category: {id: 3},
        card: { id: 2 },
        limit: 16.99,
        interval: "1m",
        amount: 16.99
    },
    {
        service: { id: 5 },
        category: {id: 3},
        card: { id: 2 },
        limit: 24.00,
        interval: "1m",
        amount: 24.99
    },
    {
        service: { id: 6 },
        category: {id: 3},
        card: { id: 1 },
        limit: 20.00,
        interval: "1m",
        amount: 19.99
    },
];