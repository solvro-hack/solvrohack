export const ConnectedServicedSeed = [
    {
        subscriptionName: "play.google.com",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Google_Play_Arrow_logo.svg/1200px-Google_Play_Arrow_logo.svg.png",
    },
    {
        subscriptionName: "Netflix.com",
        image: "https://seeklogo.com/images/N/netflix-logo-6A5D357DF8-seeklogo.com.png",
    },
    {
        subscriptionName: "Spotify.com",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/1024px-Spotify_logo_without_text.svg.png",
    },
    {
        subscriptionName: "Steam.com",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Steam_icon_logo.svg/1024px-Steam_icon_logo.svg.png",
    },
    {
        subscriptionName: "Origin.com",
        image: "https://brandslogos.com/wp-content/uploads/thumbs/origin-logo-vector.svg",
    },
    {
        subscriptionName: "LinkedIn.com",
        image: "https://image.flaticon.com/icons/png/512/174/174857.png",
    },
];