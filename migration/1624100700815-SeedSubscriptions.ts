import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";
import { SubscriptionSeed } from "../seed/subscriptions.seed";

export class SeedSubscriptions1624100700815 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<InsertResult> {
        return getRepository('subscription').insert(SubscriptionSeed);
      }
  
      public async down(_: QueryRunner): Promise<DeleteResult> {
        return getRepository('subscription').delete(SubscriptionSeed);
      }
}
