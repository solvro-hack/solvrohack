import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";

import { CategorySeed } from "../seed/category.seed";

export class SeedCategory1624073247920 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<InsertResult> {
        return getRepository('category').insert(CategorySeed);
    }

    public async down(queryRunner: QueryRunner): Promise<DeleteResult> {
        return getRepository('category').delete(CategorySeed);
    }
}

