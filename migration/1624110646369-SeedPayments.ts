import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";
import { PaymentsSeed } from "../seed/payments.seed";

export class SeedPayments1624110646369 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<InsertResult> {
        return getRepository('payment').insert(PaymentsSeed);
    }

    public async down(_: QueryRunner): Promise<DeleteResult> {
        return getRepository('payment').delete(PaymentsSeed);
    }
}
