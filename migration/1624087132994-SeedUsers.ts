import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";
import { UsersSeed } from "../seed/user.seed";

export class SeedUsers1624087132994 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<InsertResult> {
      return getRepository('user').insert(UsersSeed);
    }

    public async down(_: QueryRunner): Promise<DeleteResult> {
      return getRepository('user').delete(UsersSeed);
    }
}
