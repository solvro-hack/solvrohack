import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";
import { ConnectedServicedSeed } from "../seed/connectedServices.seed";

export class SeedConnectedServices1624091996056 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<InsertResult> {
        return getRepository('connected_service').insert(ConnectedServicedSeed);
      }
  
      public async down(_: QueryRunner): Promise<DeleteResult> {
        return getRepository('connected_service').delete(ConnectedServicedSeed);
      }

}
