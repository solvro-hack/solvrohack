import {DeleteResult, getRepository, InsertResult, MigrationInterface, QueryRunner} from "typeorm";
import { CardSeed } from "../seed/card.seed";

export class SeedCard1624087136890 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<InsertResult> {
        return getRepository('card').insert(CardSeed);
      }
  
      public async down(_: QueryRunner): Promise<DeleteResult> {
        return getRepository('card').delete(CardSeed);
      }
}
