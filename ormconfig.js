module.exports = {
   type: "postgres",
   host: process.env.DB_HOST || "postgres",
   port: process.env.DB_PORT || 5432,
   username: "postgres",
   password: "postgres",
   database: "solvrohack",
   synchronize: true,
   logging: false,
   entities: [
      "src/entity/**/*.ts"
   ],
   migrations: [
      "migration/**/*.ts"
   ]
}