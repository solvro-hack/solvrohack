
export interface IPayment {
    id: number;
    category: string;
    categoryIcon: string;
    serviceName: string;
    serviceImg: string;
    subscriptionId: number;
    amount: number;
    createdAt: Date;
}