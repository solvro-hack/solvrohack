import express from 'express';

// Extending express Request with rawBody
export interface ExtendendRequest extends express.Request {
    // eslint-disable-next-line
    [rawBody: string]: any;
}