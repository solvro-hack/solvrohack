import { IPayment } from "./payment.interface";

export interface IUserPayments {
    payments: IPayment[];
}