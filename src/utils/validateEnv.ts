import {
    cleanEnv, str, port
  } from 'envalid';
  
  function validateEnv(): void {
    cleanEnv(process.env, {
      PORT: port(),
      ENVIROMENT: str(),
      BASE_URL: str()
    });
  }
  
  export { validateEnv }