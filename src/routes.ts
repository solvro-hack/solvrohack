import { Response, Request, Router } from 'express';
import { Payment } from './entity/payment.entity';
import { PaymentController } from './payments/payment.controller';
import { SubscriptionController } from './subscription/subscriptions.controller';
import { StatusCode, statusCodes } from './utils/statusCodes';
import { IUserPayments } from './utils/userPayments.interface';


class Routes {
    public router = Router();

    constructor() {
        this.initRoutes();
        this.initExceptionRoutes();
    }

    private initRoutes = () => {
        /**
         * GET /, /ping: Basic routes to confirm that Backend works correctly.
         *
         * @returns JSON containing 200 status code.
         */
        this.router.get(['/', '/ping'], (req: Request, res: Response) => {
            res.status(200).json( statusCodes[200].json );
        });

        this.router.get('/payments/:paymentId/', async (req: Request, res: Response) => {
            try {
                const paymentId: number = parseInt(req.params.paymentId, 10);
                const paymentController = new PaymentController();
                const payment: Payment | StatusCode = await paymentController.getPayment(paymentId);
                
                res.status(200).json(payment);
                
            } catch (error) {
                res.status(500).json(statusCodes[400].json);
            }
        });

        this.router.get('/payments', async (req: Request, res: Response) => {
            try {
                if(!req.query.userId || typeof req.query.userId === 'undefined') {
                    res.status(400).json(statusCodes[400].json);
                    return;
                }
                const userId: number = parseInt(req.query.userId.toString(), 10);
                const paymentController = new PaymentController();
                const payment: IUserPayments | StatusCode = await paymentController.getUserPayments(userId);

                res.status(200).json(payment);
            } catch (error) {
                console.log(error)
                res.status(500).json(statusCodes[400].json);
            }
        });

        this.router.get('/subscriptions', async (req: Request, res: Response) => {
            try {
                const cardId = parseInt(req.query.cardId as string);
                const userId = parseInt(req.query.userId as string);
                const subscriptionController = new SubscriptionController();                
                const result = await subscriptionController.getAllSubscriptions(cardId, userId);

                if (result.length) {
                    res.status(200).json(result);
                } else {
                    res.sendStatus(404);
                }
            } catch (error) {
                console.log(error);
                
                res.status(500).json(statusCodes[400].json);
            }
        });

        this.router.get('/subscriptions/:subscriptionId', async (req: Request, res: Response) => {
            try {
                const subscriptionId = req.params.subscriptionId;
                const subscriptionController = new SubscriptionController();
                const result = await subscriptionController.getOneSubscription(subscriptionId);
                if (result) {
                    res.status(200).json(result);
                } else {
                    res.sendStatus(404);
                }
            } catch (error) {
                res.status(500).json(statusCodes[400].json);
            }
        });

        this.router.post('/subscriptions', async (req: Request, res: Response) => {
            try {
                const {cardId, serviceId, subscriptionName, amount, interval} = req.body;
                const subscriptionController = new SubscriptionController();
                const result = await subscriptionController.addSubscriptionToCard(
                    cardId,
                    serviceId,
                    subscriptionName,
                    amount,
                    interval
                );

                res.status(201).json(result);
            } catch (error) {
                res.status(500).json(statusCodes[400].json);
            }
        });

        this.router.put('/subscriptions/:subscriptionId', async (req: Request, res: Response) => {
            try {
                const subscriptionId = parseInt(req.params.subscriptionId);
                const {plannedFinish, limit, amount, interval} = req.body;
                const subscriptionController = new SubscriptionController();
                const result = await subscriptionController.updateubscriptionToCard(
                    subscriptionId, plannedFinish, limit, interval, amount,
                );

                res.status(200).json(result);
                
            } catch (error) {
                res.status(500).json(statusCodes[400].json);
            }
        });
    }

    private initExceptionRoutes = () => {

        /**
         * Catch all POST routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.post('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all PUT routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.put('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all HEAD routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.head('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all DELETE routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.delete('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all CONNECT routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.connect('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all PATCH routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.patch('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all TRACE routes to defined GET routes
         *
         * @returns JSON containing 405 status code.
         */
        this.router.trace('*', (req: Request, res: Response) => {
            res.status(405).json(statusCodes[405].json);
        });

        /**
         * Catch all non-registered GET routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.get('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });

        /**
         * Catch all non-registered POST routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.post('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });

        /**
         * Catch all non-registered PUT routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.put('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });


        /**
         * Catch all non-registered HEAD routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.head('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });


        /**
         * Catch all non-registered DELETE routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.delete('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });

        /**
         * Catch all non-registered CONNECT routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.connect('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });

        /**
         * Catch all non-registered TRACE routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.trace('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });

        /**
         * Catch all non-registered PATCH routes
         *
         * @returns JSON containing 404 status code.
         */
        this.router.patch('*', (req: Request, res: Response) => {
            res.status(404).json(statusCodes[404].json);
        });
    }
}

export { Routes };