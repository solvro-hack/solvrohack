import express from 'express';
import { AddressInfo } from 'net';
import { ExtendendRequest } from './utils/utils';
import { Routes } from './routes';
import "reflect-metadata";


class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.initMiddleware();
        this.initRouter();
    }

    public listen = (): void => {
        const server = this.app.listen(+(process.env.PORT || 800), (process.env.BASE_URL || '0.0.0.0'), () => {
            const { port, address } = server.address() as AddressInfo;
            console.log(`Server listening on: http://${address}:${+port}`);
        });
    }

    private initMiddleware = (): void => {
        this.app.use(express.json({
            limit: '50mb',
            verify(req: ExtendendRequest, res: express.Response, buf: Buffer, encoding: BufferEncoding): void {
                if( buf && buf.length ) {
                    req.rawBody = buf.toString(encoding || 'utf-8');
                }
            }
        }));
    }

    private initRouter = (): void => {
        const routes = new Routes();
        this.app.use("/", routes.router);
    }
}


export { App }