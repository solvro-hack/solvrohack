import { validateEnv } from './utils/validateEnv';
import { App } from './app';
import 'dotenv/config';
import { createConnection } from 'typeorm';

validateEnv();

createConnection().then(async (conn) => {
    await conn.synchronize();
    await conn.runMigrations();

    const app = new App();

    app.listen();
}).catch(console.error);