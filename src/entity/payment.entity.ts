import { Entity, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, Column } from "typeorm";
import { Card } from "./card.entity";
import { Category } from "./category.entity";
import { Subscription } from "./subscription.entity";
import { User } from "./user.entity";

@Entity({ name: 'payment' })
export class Payment {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Subscription, subscription => subscription.payments, { eager: true })
    subscription: Subscription;

    @ManyToOne(type => Card, card => card.id)
    card: Card;

    @Column({ type: 'decimal' })
    amount: number;

    @ManyToOne(type => Category, category => category.id, { eager: true })
    category: Category;

    @ManyToOne(type => User, user => user.payments)
    user: User;

    @CreateDateColumn()
    created_at: Date;

}