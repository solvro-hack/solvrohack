import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Payment } from "./payment.entity";

@Entity({ name: 'category' })
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'category_name', type: 'varchar', length: 255 })
    categoryName: string;

    @Column({ type: 'varchar', length: 255 })
    icon: string;

    @OneToMany(type => Payment, payment => payment.category)
    payment: Payment[];
}