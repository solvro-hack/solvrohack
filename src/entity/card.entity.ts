import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity({ name: 'card' })
export class Card {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255 })
    number: string;

    @Column({ type: 'smallint' })
    cvc: number;

    @Column({ type: 'varchar', length: 255 })
    type: string;

    @ManyToOne(type => User, user => user.cards)
    user: User;
}