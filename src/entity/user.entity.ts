import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Card } from "./card.entity";
import { Payment } from "./payment.entity";

@Entity({ name: 'user' })
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'first_name', type: 'varchar', length: 255 })
    firstName: string;

    @Column({ name: 'last_name', type: 'varchar', length: 255 })
    lastName: string;

    @OneToMany(type => Payment, payment => payment.user, {eager: true})
    payments: Payment[];

    @OneToMany(type => Card, card => card.user)
    cards: Card[];
}