import { Payment } from "./payment.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { Card } from "./card.entity";
import { ConnectedService } from "./connectedService.entity";
import { Category } from "./category.entity";

@Entity({ name: 'subscription' })
export class Subscription {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => ConnectedService, service => service.id, { eager: true })
    service: ConnectedService;
    
    @ManyToOne(type => Category, category => category.id, { eager: true })
    category: Category;

    @ManyToOne(type => Card, card => card.id, { eager: true })
    card: Card;

    @Column({ type: 'decimal', nullable: true })
    limit: number|null;

    @Column({ type: 'varchar', length: 16 })
    interval: string;

    @Column({ type: 'decimal' })
    amount: number;

    @Column({ type: 'date', nullable: true })
    plannedFinish: Date | null;

    @OneToMany(type => Payment, payments => payments.subscription)
    payments: Payment[];

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @DeleteDateColumn()
    deleted_at: Date;

}