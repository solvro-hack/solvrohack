import { statusCodes, StatusCode } from "../utils/statusCodes";
import { Payment } from "../entity/payment.entity";
import { User } from "../entity/user.entity";
import { getRepository } from "typeorm";
import { IUserPayments } from "../utils/userPayments.interface";
import { IPayment } from "../utils/payment.interface";


export class PaymentController {

    private paymentRepository = getRepository(Payment);
    private userRepository = getRepository(User);

    // Info about one payment
    async getPayment(paymentId: number): Promise<Payment | StatusCode> {
        const payment = await this.paymentRepository.findOne(paymentId);
        if(payment && typeof payment !== undefined) {
            return payment;
        }

        return statusCodes[404];
    }

    // History of all payments for user
    async getUserPayments(userId: number): Promise<IUserPayments | StatusCode> {
        
        const user: User | undefined = await this.userRepository.findOne(userId);

        if(user && typeof user !== undefined) {
            const userPayments: Payment[] | undefined = await this.paymentRepository.find({where: {user: {id: user.id}}, order: {"created_at": "DESC"}});
            const payments: IPayment[] = this.mapPaymentsToIPayments(userPayments); 
            return { payments: payments };
        }

        return statusCodes[401];
    }

    // Info about future payments
    async getFuturePayments(userId: number): Promise<Payment[] | StatusCode> {
        const user = await this.userRepository.findOne(userId);

        if(user && typeof user !== undefined) {
            return user.payments;
        }

        return statusCodes[401];
    }

    mapPaymentsToIPayments(payments: Payment[]): IPayment[] {
        const result = payments.map( function(payment) {
            return {
                id: payment.id,
                category: payment.category.categoryName,
                categoryIcon: payment.category.icon,
                serviceName: payment.subscription.service.subscriptionName,
                serviceImg: payment.subscription.service.image,
                subscriptionId: payment.subscription.id,
                amount: payment.subscription.amount,
                createdAt: payment.created_at
            } as IPayment
        });

        return result;
    }

}