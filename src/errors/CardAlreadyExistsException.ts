export class CardAlreadyExistsException extends Error {
    constructor() {
        super("Card already exists for provided user");
        Object.setPrototypeOf(this, CardAlreadyExistsException.prototype);
    }
}