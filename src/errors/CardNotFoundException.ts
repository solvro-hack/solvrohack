export class CardNotFoundException extends Error {
    constructor() {
        super("User with provided card not found");
        Object.setPrototypeOf(this, CardNotFoundException.prototype);
    }
}