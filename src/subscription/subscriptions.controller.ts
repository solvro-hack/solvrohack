import { getRepository, InsertResult } from "typeorm";
import { Subscription } from "../entity/subscription.entity";
import { Card } from "../entity/card.entity";
import { Payment } from "../entity/payment.entity";


export class SubscriptionController {

    private subscriptionRepository = getRepository(Subscription);
    private paymentRepository = getRepository(Payment);
    private cardRepository = getRepository(Card);

    async getAllSubscriptions(cardId: number, userId: number) {
        let where: any = {}
        if (userId) {
            const cards = await this.cardRepository.find({ where: { user: { id: userId } } });
            where = cards.map(card => ({ card: { id: card.id } }))
        } else if (userId) {
            where.card = { id: cardId };
        }

        const results = await this.subscriptionRepository.find({ where });
        return Promise.all(results.map(async ({
            id,
            category: { categoryName, icon },
            limit,
            interval,
            amount,
            plannedFinish,
            created_at,
            deleted_at, 
            service: { subscriptionName, image },
            card: { id: cardId, number, type },
        }) => ({
                id,
                serviceName: subscriptionName,
                serviceImg: image,
                nextTransaction: await this.getNextPayment(id, interval, created_at),
                costSum: await this.getSumOfTransactions(id),
                amount,
                plannedFinish,
                limit,
                categoryName,
                categoryIcon: icon,
                card: { id: cardId, number, type },
                createdAt: created_at,
                deletedAt: deleted_at,
            }
        )));
    }

    private async getNextPayment(subscriptionId: number, interval?: string, subscriptionCreatedAt?: Date) {
        if (!interval || !subscriptionCreatedAt) {
            const {interval: newInterval, created_at: newCreatedAt } = await this.subscriptionRepository.findOneOrFail({ where: { id: subscriptionId} });
            interval = newInterval;
            subscriptionCreatedAt = newCreatedAt;
        }

        let lastPayment: { created_at: Date } | undefined = await this.paymentRepository.findOne({
            where: { subscription: { id: subscriptionId } },
            order: {'created_at': 'DESC'}
        });
        if (!lastPayment) lastPayment = { created_at: subscriptionCreatedAt };

        let nextPayment = new Date(lastPayment.created_at);
        if (/.*d/.test(interval)) {
            const [days] = interval.match(/\d+/) || ["0"];
            nextPayment.setDate(nextPayment.getDay() + parseInt(days));
        } else if (/.*m/.test(interval)) {
            const [months] = interval.match(/\d+/) || ["0"];
            nextPayment.setMonth(nextPayment.getMonth() + parseInt(months));
        } else if (/.*y/.test(interval)) {
            const [years] = interval.match(/\d+/) || ["0"];
            nextPayment.setFullYear(nextPayment.getFullYear() + parseInt(years));
        }
        return nextPayment;
    }

    private async getSumOfTransactions(subscriptionId: number) {
        const [sum] = await this.paymentRepository
            .createQueryBuilder("payments")
            .select("SUM(payments.amount)", "sum")
            .where("payments.subscriptionId = :subscriptionId", { subscriptionId })
            .getRawOne();
        return sum;
    }

    async getOneSubscription(
        subscriptionId: string,
    ): Promise<Subscription | undefined> {
        return this.subscriptionRepository.findOne(subscriptionId);
    }

    async addSubscriptionToCard(
        cardId: number,
        serviceId: number,
        subscriptionName: string,
        amount: number,
        interval: string
    ): Promise<InsertResult> {
        return await this.subscriptionRepository.insert(
            Object.assign(new Subscription(), {
                card: { id: cardId },
                service: { id: serviceId },
                interval,
                amount,
                subscriptionName,
            }
        ));
    }

    async updateubscriptionToCard(
        subscriptionId: number,
        limit: number,
        interval: string,
        plannedFinish: Date,
        amount: number
    ) {
        return await this.subscriptionRepository.update(
            subscriptionId,
            {
                plannedFinish,
                limit,
                interval,
                amount,
            }
        )
    }
}