### Solvro Hack backend app

Build with TypeScript, NodeJS, Express

## 1. Installation

 * Run `npm install`
 * Copy .env.example file and paste it as new file `.env`
 * Set .env variable values
 * run `npm run start`